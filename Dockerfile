# Build Stage
FROM golang:1.17 AS build-stage

LABEL app="build-FakerApp"
LABEL REPO="https://gitlab.com/nishanth.shetty.netbook/FakerApp"

ENV PROJPATH=/go/src/gitlab.com/nishanth.shetty.netbook/FakerApp

# Because of https://github.com/docker/docker/issues/14914
ENV PATH=$PATH:$GOROOT/bin:$GOPATH/bin

ADD . /go/src/gitlab.com/nishanth.shetty.netbook/FakerApp
WORKDIR /go/src/gitlab.com/nishanth.shetty.netbook/FakerApp

RUN make build-alpine

# Final Stage
FROM alpine:latest

ARG GIT_COMMIT
ARG VERSION
LABEL REPO="https://gitlab.com/nishanth.shetty.netbook/FakerApp"
LABEL GIT_COMMIT=$GIT_COMMIT
LABEL VERSION=$VERSION

# Because of https://github.com/docker/docker/issues/14914
ENV PATH=$PATH:/opt/FakerApp/bin

WORKDIR /opt/FakerApp/bin

COPY --from=build-stage /go/src/gitlab.com/nishanth.shetty.netbook/FakerApp/bin/FakerApp /opt/FakerApp/bin/
RUN chmod +x /opt/FakerApp/bin/FakerApp

# Create appuser
RUN adduser -D -g '' FakerApp
USER FakerApp

ENTRYPOINT ["/opt/FakerApp/bin/FakerApp"]
