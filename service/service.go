package service

import (
	"context"
	"fmt"

	fakerapp "gitlab.com/nishanth.shetty.netbook/FakerApp/proto"
	"gitlab.com/nishanth.shetty.netbook/FakerApp/config"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/genproto/googleapis/api/httpbody"
	"github.com/go-coldbrew/errors"
	"github.com/go-coldbrew/log"
)

type svc struct {
	prefix string
}

//ReadinessProbe for the service
func (s *svc) ReadyCheck(ctx context.Context, _ *emptypb.Empty) (*httpbody.HttpBody, error) {
	return GetReadyState(ctx)
}

//LivenessProbe for the service
func (s *svc) HealthCheck(ctx context.Context, _ *empty.Empty) (*httpbody.HttpBody, error) {
	return GetHealthCheck(ctx), nil
}

func (s *svc) Echo(_ context.Context, req *fakerapp.EchoRequest) (*fakerapp.EchoResponse, error) {
	return &fakerapp.EchoResponse{
		Msg: fmt.Sprintf("%s: %s", s.prefix, req.GetMsg()),
	}, nil
}

func (s *svc) Error(ctx context.Context, req *fakerapp.EchoRequest) (*fakerapp.EchoResponse, error) {
	err := errors.New("This is an Error")
	log.Info(ctx, "error requested")
	return nil, errors.Wrap(err, "endpoint error")
}

// Creates a new Service
func New(cfg config.Config) (fakerapp.FakeSvcServer, error) {
	s := &svc{
		prefix: cfg.Prefix,
	}
	SetReady() // service initialized we can now serve traffic
	return s, nil

}
